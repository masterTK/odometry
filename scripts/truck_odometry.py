#! /usr/bin/env python3

import rospy
import time
import math
import csv
import numpy as np
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Pose, PoseWithCovariance,PoseWithCovarianceStamped,Twist,TwistWithCovariance, TwistWithCovarianceStamped,Point, PoseStamped
from geometry_msgs.msg import TransformStamped
from std_msgs.msg import Header
from tf.transformations import quaternion_from_euler
import tf
from truck_msgs.msg import zf_encoder
import matplotlib.pyplot as plt
from nav_msgs.msg import Path


def encoder_cb(msg):
	"""subscriber callback to retrieve the encoder message"""
	global front_left_encoder ,front_right_encoder,count,csv_writer,node_shutdown,encoderTimestamp
	if (msg.left == -1) or (msg.right == -1): #fail safe to check encoder value
		#print(type(msg.left))
		rospy.logerr("Encoder values are not available....")
		rospy.loginfo("Left: {} Right: {}".format(msg.left,msg.right))
		rospy.loginfo("Shutting down the node for encoder odometry....")
		node_shutdown=True
	else:
		node_shutdown = False
	
	front_left_encoder = msg.left
	front_right_encoder = msg.right
	encoderTimestamp = msg.header.stamp.to_sec()
	
def encoder_csv():
	global csv_writer,count
	count = 0
	#print("count",count)
	path = "/home/bala-au/Documents/thesis/Sensor_fusion/documents/encoder_analysis/wheel_"
	file_path = path+"encoder_left_right.csv"
	#dif_file = path+"diff_file.csv"
	file = open(file_path,"w")
	#d_file = open(dif_file,"w")
	csv_writer = csv.writer(file)
	#dif_writer = csv.writer(d_file)
	
	#dif_writer.writerow(["count","left","right"])
	csv_writer.writerow(["Count","LEFT","RIGHT"])

def get_front_axle_frame_pose():
	x = rospy.get_param("~front_axle_frame_x")
	y = rospy.get_param("~front_axle_frame_y")
	return (x,y)

def get_odom_pose(x,y):

	front_axle_frame_x,front_axle_frame_y = get_front_axle_frame_pose()
	x = front_axle_frame_x + x
	y = front_axle_frame_y + y

	return (x,y)

def twist_msg():
	""" returns the ros message for Twist"""
	twist = Twist()
	twist.linear.x = 0.0
	twist.linear.y = 0.0
	twist.linear.z = 0.0

	twist.angular.x = 0.0
	twist.angular.y = 0.0
	twist.angular.z = 0.0

	return twist

def pose_msg(x,y,theta):
	""" returns the ros message for Pose"""
	pose = Pose()
	
	#translation position
	pose.position.x = x
	pose.position.y = y
	pose.position.z = 0.0

	#orientation
	rot_quat = pose.orientation
	rot_quat.x,rot_quat.y,rot_quat.z,rot_quat.w = quaternion_from_euler(0.0,0.0,theta)
	
	return pose

def getRosPoseMsg(seq,x,y,theta):

	msg = PoseWithCovarianceStamped()
	msg.header.seq = seq
	msg.header.stamp = rospy.Time.now()
	msg.header.frame_id = "base_link"

	msg.pose.pose = pose_msg(x,y,theta)


	msg.pose.covariance = np.zeros(36)

	return msg

def get_odom_message(seq,x,y,theta, v, yawrate):
	""" returns the ros message for Odometry"""

	header = Header()
	header.seq = seq
	header.stamp = rospy.Time.now()
	header.frame_id = "odom"

	pose_cov = PoseWithCovariance()
	pose_cov.pose = pose_msg(x, y, theta)
	pose_cov.covariance[0] = 0.055620478
	pose_cov.covariance[7] = 1.081492998
	pose_cov.covariance[14] = 1000000000000.0
	pose_cov.covariance[21] = 1000000000000.0
	pose_cov.covariance[28] = 1000000000000.0
	pose_cov.covariance[35] = 0.51654999

	twist_cov = TwistWithCovariance()
	twist_cov.twist = twist_msg()
	twist_cov.covariance[0] = 1000000000000.0
	twist_cov.covariance[7] = 1000000000000.0
	twist_cov.covariance[14] = 1000000000000.0
	twist_cov.covariance[21] = 1000000000000.0
	twist_cov.covariance[28] = 1000000000000.0
	twist_cov.covariance[35] = 1000000000000.0
	odom = Odometry()
	odom.header = header
	odom.child_frame_id = "base_link"
	odom.pose = pose_cov
	odom.twist = twist_cov

	return odom

def get_change_rotation(enc_difference):

	if enc_difference > 4096/2:
		enc_difference = enc_difference - 4096
	elif enc_difference < -4096/2:
		enc_difference = enc_difference + 4096

	diff_rotation = enc_difference/4096.0
	
	return diff_rotation
	
def get_wheel_distance_travelled(encoder_change,wheel_diameter):

	if abs(encoder_change) > 2:
		#how much the wheel has rotated
		diff_rotation = get_change_rotation(encoder_change)

		#distance travelled
		distance_travelled = diff_rotation * wheel_diameter * math.pi
	else:
		distance_travelled = 0.0

	return distance_travelled

def checkActiveSensors():
	global encoder_active,oldEncoderTimeStamp,encoderTimestamp,pozyx_active,\
	oldPozyxTimeStamp, pozyxTimeStamp, prePozyxData, pozyxMeas, filterTimeStamp
	
	if(((encoderTimestamp - oldEncoderTimeStamp) > 0.0125) and (front_left_encoder!=-1) and (front_right_encoder!=-1)):
		encoder_active = True

def calculate_truck_pose(wheel_diameter,wheel_base,wheel_track,rate):
	global front_left_encoder,front_right_encoder,node_shutdown,encoderTimestamp,oldEncoderTimeStamp,\
			encoder_active

	myPath = Path()

	x = rospy.get_param("~initial_x",2.071)#in metres
	y = rospy.get_param("~initial_y",7.128)
	yaw = rospy.get_param("~initial_theta",0.0)
	
	rospy.loginfo("Initialised the starting pose of the truck as follows")
	rospy.loginfo("X: {} Y: {} Theta: {}".format(x,y,yaw))


	rospy.loginfo("Waiting to receive the encoder values on the topic [/encoder]")
	if rospy.wait_for_message("/encoder",zf_encoder):
		rospy.loginfo("Receiving the encoder values for calculating truck pose")
		rospy.loginfo("Starting to calculate the truck pose from encoder")
	#change in pose
	dx = 0.0
	dy = 0.0
	dyaw = 0.0

	#recording the values before the loop for calculation
	pre_left_encoder = front_left_encoder
	pre_right_encoder = front_right_encoder
	oldEncoderTimeStamp = encoderTimestamp
	encoder_active = False

	#odom_publisher = rospy.Publisher("zf_inno_truck/pose/encoder/odom",Odometry,queue_size=10)
	odom_publisher = rospy.Publisher("/zf_inno_truck/encoder/odometry",PoseWithCovarianceStamped,queue_size=10)
	path_publisher = rospy.Publisher("/zf_inno_truck/encoder/path", Path, queue_size=1)
	twist_publisher = rospy.Publisher("/zf_inno_truck/encoder/twist", TwistWithCovarianceStamped, queue_size=1)
	#enc_dist_publisher = rospy.Publisher("/zf_inno_truck/encoder/distances",Point,queue_size=10)

	seq = 0 #sequence of the topic message

	while not rospy.is_shutdown() and not node_shutdown:
		checkActiveSensors()
		#recording the current_values for odometry calculation
		curr_left_encoder = front_left_encoder
		curr_right_encoder = front_right_encoder

		if encoder_active:
			
			diff_left_encoder = pre_left_encoder - curr_left_encoder  
			diff_right_encoder = -(pre_right_encoder - curr_right_encoder)

			#how much the distance each wheel has travelled
			wheel_left_dis_travelled = get_wheel_distance_travelled(diff_left_encoder,wheel_diameter)
			wheel_right_dis_travelled = get_wheel_distance_travelled(diff_right_encoder,wheel_diameter)

			#distance travelled by the vehicle
			vehicle_dis_travelled = (wheel_left_dis_travelled + wheel_right_dis_travelled)/2.0
			dt = oldEncoderTimeStamp - encoderTimestamp
			
			dyaw = (wheel_right_dis_travelled - wheel_left_dis_travelled)/wheel_track
			dx = vehicle_dis_travelled * np.cos(yaw + (dyaw/2.0))
			dy = vehicle_dis_travelled * np.sin(yaw + (dyaw/2.0))

			v = vehicle_dis_travelled / dt
			vx = dx/dt
			vy = dy/dt
			yaw_rate = dyaw / dt
			
			#update current pose
			x+=dx
			y+=dy
			yaw+=dyaw
			print("X: {} Y:{} Theta:{} ".format(x,y,yaw))

					
			myTwist = TwistWithCovarianceStamped()
			myTwist.header.stamp = rospy.Time.now()
			myTwist.header.frame_id = 'odom'

			linear = myTwist.twist.twist.linear
			linear.x = -v
			linear.y = 0.0
			linear.z = 0.0

			angular = myTwist.twist.twist.angular
			angular.z = -yaw_rate

			myTwist.twist.covariance[0] = 0.2
			myTwist.twist.covariance[7] = 0.2
			myTwist.twist.covariance[-1] = 0.4

			twist_publisher.publish(myTwist)

			
		else:
			print("No new data from encoder")
		
		seq += 1
		pose_odom = getRosPoseMsg(seq,x,y,yaw)

		myPath.header.stamp = rospy.Time.now()
		myPath.header.frame_id = 'odom'

		myPose = PoseStamped()
		myPose.header.stamp = rospy.Time.now()
		myPose.header.frame_id = 'odom'
		myPose.pose.position = pose_odom.pose.pose.position
		myPose.pose.orientation = pose_odom.pose.pose.orientation

		myPath.poses.append(myPose)

		path_publisher.publish(myPath)

		#publish the message on the topic
		odom_publisher.publish(pose_odom)

		#update the previous encoder values for next iteration
		pre_left_encoder = curr_left_encoder
		pre_right_encoder = curr_right_encoder
		oldEncoderTimeStamp = encoderTimestamp
		encoder_active = False

		rate.sleep()

rospy.init_node("encoder_odometry")

rate = rospy.Rate(30)
 
rospy.Subscriber("/encoder",zf_encoder,encoder_cb)

wheel_diameter = rospy.get_param("~wheel_diameter",109.0)/1000.0 #in metres
wheel_base = rospy.get_param("~wheel_base",437.0)/1000.0
wheel_track = rospy.get_param("~wheel_track",269.0)/1000.0

calculate_truck_pose(wheel_diameter,wheel_base,wheel_track,rate)
