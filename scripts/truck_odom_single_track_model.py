#! /usr/bin/env python3

import rospy
from truck_msgs.msg import zf_encoder
from geometry_msgs.msg import PoseWithCovarianceStamped, PoseStamped, TwistWithCovarianceStamped
from nav_msgs.msg import Path, Odometry
from std_srvs.srv import Trigger
import math
import numpy as np
from tf.transformations import quaternion_from_euler
from sensor_msgs.msg import NavSatFix
import utm
import pandas as pd
class odom_single_track:
    def __init__(self) -> None:
        self.wheelbase = rospy.get_param('~wheelbase', default=0.4390)
        self.ticks_per_revolution = rospy.get_param('~encoder_ticks', default=4096)
        self.wheel_diameter = rospy.get_param('wheel_diameter', default=0.1073)
        self.track_width = 0.269


        self.sub_encoder = rospy.Subscriber('/encoder', zf_encoder, self.cb_encoder, queue_size=10)
        self.sub_gps = rospy.Subscriber('/ublox_gps/fix', NavSatFix, self.cb_gps, queue_size=10)
        self.path_pub = rospy.Publisher('/zf_inno_truck/encoder/ackermann/path', Path, queue_size=10)
        self.odom_pub = rospy.Publisher('/zf_inno_truck/encoder/ackermann/pose', PoseWithCovarianceStamped, queue_size=10)
        self.twist_pub = rospy.Publisher('/zf_inno_truck/encoder/ackermann/twist', TwistWithCovarianceStamped, queue_size=10)
        self.srv_reset = rospy.Service('/zf_inno_truck/encoder/reset',Trigger, self.cb_reset)
        self.path_gps_pub = rospy.Publisher('/gps/path', Path, queue_size=10)
        self.sub_kalman = rospy.Subscriber('/odometry/filtered', Odometry, self.cb_kalman)

        self.list_odom = []
        self.list_kalman = []


        self.last_rear_tick = 0
        self.last_steering_tick = 0
        self.last_timestamp = 0

        self.neutral_steering_tick = 3003#3466 - 120
        self.rear_to_cog = self.wheelbase / 2

        self.counter = 0
        self.counter_dt = 0

        self.x = 0
        self.y = 0
        self.theta = 0
        self.delta = 0

        self.path_msg = Path()
        self.path_msg.header.frame_id = 'odom'

        self.path_gps_msg = Path()
        self.path_gps_msg.header.frame_id = 'odom'

        # self.correction_factor_right = 0.77
        # self.correction_factor_left = 1.31        
        self.correction_factor_right = 0.77
        self.correction_factor_left = 1.47

        self.max_steer_right = math.radians(25.630 * self.correction_factor_right)
        self.max_steer_left = math.radians(16.495 * self.correction_factor_left)

        self.max_steer_right_ticks = 2879
        self.max_steer_left_ticks = 3890

        self.angle_per_tick_left = self.max_steer_left / abs(self.neutral_steering_tick - self.max_steer_left_ticks)
        self.angle_per_tick_right = self.max_steer_right / abs(self.neutral_steering_tick - self.max_steer_right_ticks)

        self.rs = []

    def cb_kalman(self, msg:Odometry):
        self.list_kalman.append([msg.pose.pose.position.x, msg.pose.pose.position.y])

    def cb_gps(self, msg:NavSatFix):
        origin = np.array([533503.16563755, 5279268.88998422])
        utm_x, utm_y, _,_ = utm.from_latlon(msg.latitude, msg.longitude)
        p = np.array([utm_x, utm_y]) - origin

        rangl = np.radians(82.9722746)

        rot_mat = np.array([[np.cos(rangl), -np.sin(rangl)], [np.sin(rangl), np.cos(rangl)]])

        p_trans = rot_mat @ p       

        pose = PoseStamped()
        pose.header.frame_id ='odom'
        pose.pose.position.x = p_trans[0]
        pose.pose.position.y = p_trans[1]
        self.path_gps_msg.poses.append(pose)

        self.path_gps_pub.publish(self.path_gps_msg)

    def cb_reset(self, req):
        self.list_odom
        df_odom = pd.DataFrame(self.list_odom, columns=['x', 'y'])
        df_odom.to_csv('/home/zdm/eval_odometry/df_odom.csv')

        self.list_kalman
        df_kalman = pd.DataFrame(self.list_kalman, columns=['x', 'y'])
        df_kalman.to_csv('/home/zdm/eval_odometry/df_kalman.csv')


        self.list_kalman = []
        self.list_odom = []

        self.x = 0
        self.y = 0
        self.theta = 0
        self.delta = 0
        self.path_msg = Path()
        self.path_msg.header.frame_id = 'odom'

        #print(self.rs)



        return True, "States resetted!"

    def single_track_model(self, msg: zf_encoder, dt):
        d_ticks = self.get_true_delta(msg.back, self.last_rear_tick)
        v = ((d_ticks * self.wheel_diameter * math.pi) / (self.ticks_per_revolution)) / dt
        
        #dsteer = (self.get_true_delta(msg.steering, self.last_steering_tick) * 2 * math.pi) / self.ticks_per_revolution
        #dsteer = self.last_steering_tick - msg.steering

        steer_diff = -self.get_true_delta(msg.steering, self.last_steering_tick)
        angle_per_tick = self.get_angle_per_tick(msg.steering)

        dsteer = steer_diff * angle_per_tick
        #print(math.degrees(dsteer))

        dsteerdt = dsteer / dt # == Phi
        #self.delta = (self.get_true_delta(msg.steering, self.neutral_steering_tick) * 2*math.pi) / self.ticks_per_revolution
        #self.delta = (self.neutral_steering_tick - msg.steering) * angle_per_tick
        #print(self.delta)

        side_slip = np.arctan((self.rear_to_cog * np.tan(self.delta)) / self.wheelbase)
        R = self.wheelbase / (np.tan(self.delta) * np.cos(side_slip))

        dxdt = v * np.cos(side_slip + self.theta)
        dydt = v * np.sin(side_slip + self.theta)
        dthetadt = v / R
        dphidt = dsteerdt

        self.x += dxdt * dt
        self.y += dydt * dt
        self.theta += dthetadt * dt
        self.delta += dphidt * dt

        # if abs(yaw_rate) < 0.000001:
        #     self.x += math.cos(self.theta) * v * dt
        #     self.y += math.sin(self.theta) * v * dt

        # else:
        #     delta_theta = yaw_rate * dt
        #     self.x += (v / yaw_rate) * (math.sin(self.theta + delta_theta) - math.sin(self.theta))
        #     self.y += (v / yaw_rate) * (math.cos(self.theta) - math.cos(self.theta + delta_theta))
        #     self.theta += delta_theta


        #print("{:8.4f}, {:8.4f}, {}".format(np.degrees(self.delta), np.degrees(dsteer), msg.steering))
        #print(math.degrees(self.delta))
        self.publish_pose()
        self.publish_twist(dxdt, dsteerdt, dydt)
        

    def differential_drive_model(self, msg: zf_encoder, dt):
        # Back Wheel
        k_d = 0.00177**2
        k_theta = (5**2/360)
        k_drift = 0.0698

        cf = 0.987
        d_ticks_back = self.get_true_delta(msg.back, self.last_rear_tick)
        ds = ((d_ticks_back * self.wheel_diameter *cf * math.pi) / (self.ticks_per_revolution))
        v = ds / dt
        # #self.x += ds

        # if ds > 0.02:
        #     self.counter_dt += dt
        #     self.counter+= 1
        #     print(self.counter_dt, self.counter)

        #Front Wheels:
        d_ticks_fl = self.get_true_delta(msg.left, self.last_fl_tick)
        d_ticks_fr = self.get_true_delta(msg.right, self.last_fr_tick)

        ds_fl = ((d_ticks_fl * self.wheel_diameter *cf* math.pi) / (self.ticks_per_revolution))
        ds_fr = -((d_ticks_fr * self.wheel_diameter *cf* math.pi) / (self.ticks_per_revolution))

        v_fl = ds_fl / dt
        v_fr = ds_fr / dt

        x, y, theta = self.x, self.y, self.theta

        theta_new = self.theta

        if abs(ds_fl - ds_fr) < 0.0000001:
            vx = v*np.cos(self.theta)
            vy = v*np.sin(self.theta)

            x_new = x + vx*dt
            y_new = y+ vy*dt
            yaw_rate = 0
            theta_new = theta

        else:
            #R = self.track_width *(ds_fl + ds_fr) / (2*(ds_fr - ds_fl))
            #cf =1
            if v_fl > v_fr: #Rechtskurve
                cf = 0.80299   #<----- Tuning, circle
                #cf = 0.5
                #cf = 0.93      #<----- Tuning, half circle
                #cf = 0.1
            else: 
                #cf = 1.03    #<--- TUning Wert
                cf = 1.5
                #cf = 0.1

            track_width = cf * self.track_width

            R = (track_width / 2)*((v_fl + v_fr) / (v_fr - v_fl))
            self.rs.append(R)
            wd = (v_fr - v_fl) / track_width

            icc = np.array([self.x - R*np.sin(self.theta), self.y + R*np.cos(self.theta)])

            x_new = np.cos(wd*dt) * (x - icc[0]) - np.sin(wd*dt) * (y-icc[1]) + icc[0]
            y_new = np.sin(wd*dt) * (x - icc[0]) + np.cos(wd*dt) * (y-icc[1]) + icc[1]

            vx = (x_new - x)/dt
            vy = (y_new - y)/dt
            theta_new = theta + wd*dt

            yaw_rate = wd

        self.list_odom.append([x_new, y_new])

        var_vx = (k_d / dt)*np.abs(vx)
        var_vy = 10#(k_d / dt)*np.abs(vy)
        var_yaw_rate = ((k_theta / dt) * np.abs(yaw_rate) + (k_drift / dt) * np.abs(v))*100
        #     self.x += R * np.sin(wd + self.theta) - R * np.sin(self.theta)
        #     self.y += R * np.cos(wd + self.theta) + R * np.cos(self.theta)
        #     self.theta = (self.theta + wd)
        self.x, self.y, self.theta = x_new, y_new, theta_new
        print("dt: {}, vx: {:.2f}, vy: {:.2f}, heading: {:3F}".format(dt, vx, vy, np.degrees(self.theta)))
        self.publish_twist(v, yaw_rate, vy=vy/100, vars=(var_vx, var_vy, var_yaw_rate))
        self.publish_pose()


    def cb_encoder(self, msg: zf_encoder):
        if self.last_timestamp > 0:
            dt = msg.header.stamp.to_sec() - self.last_timestamp
            if dt < 0.5:    
                #self.single_track_model(msg, dt)
                self.differential_drive_model(msg, dt)
                
            else:
                rospy.logerr("to much time between two timestamps...{}".format(dt))

        self.last_rear_tick = msg.back
        self.last_steering_tick = msg.steering
        self.last_fl_tick = msg.left
        self.last_fr_tick = msg.right
        self.last_timestamp = msg.header.stamp.to_sec()

    def publish_pose(self):
        pose_msg = PoseStamped()
        pose_msg.header.stamp = rospy.Time.now()
        pose_msg.header.frame_id = 'odom'
        pose_msg.pose.position.x = self.x
        pose_msg.pose.position.y = self.y

        rot_quat = pose_msg.pose.orientation
        rot_quat.x, rot_quat.y, rot_quat.z, rot_quat.w = quaternion_from_euler(0.0,0.0,self.theta)

        self.path_msg.poses.append(pose_msg)
        self.path_pub.publish(self.path_msg)

        pose_cov_msg = PoseWithCovarianceStamped()
        cov = np.identity(6)
        cov_vals = np.array([0.2, 0.2, 0, 0, 0, 0.4]).T

        cov = cov * cov_vals
        cov_flat = cov.ravel().tolist()
        pose_cov_msg.pose.pose = pose_msg.pose
        pose_cov_msg.pose.covariance = cov_flat
        pose_cov_msg.header.stamp = rospy.Time.now()
        pose_cov_msg.header.frame_id = 'odom'

        self.odom_pub.publish(pose_cov_msg)

    def publish_twist(self, vx, yaw_rate, vy=0, vars=(0,0,0)):
        twist_msg = TwistWithCovarianceStamped()
        twist_msg.header.stamp = rospy.Time.now()
        twist_msg.header.frame_id = 'base_link'

        twist_msg.twist.twist.linear.x = vx
        twist_msg.twist.twist.linear.y = vy
        twist_msg.twist.twist.angular.z = yaw_rate
        cov = np.identity(6)
        #cov_vals = np.array([0.01323, 0.0123, 0, 0, 0, 0.003]).T
        cov_vals = np.array([vars[0], vars[1], 0, 0, 0, vars[2]])
        cov = cov * cov_vals
        cov_flat = cov.ravel().tolist()

        twist_msg.twist.covariance = cov_flat
        self.twist_pub.publish(twist_msg)
        

    def get_true_delta(self, current_tick, last_tick):
        diff = last_tick - current_tick

        if diff > self.ticks_per_revolution / 2:
            diff -= self.ticks_per_revolution
        elif diff < -self.ticks_per_revolution / 2:
            diff += self.ticks_per_revolution

        return diff

    def get_angle_per_tick(self, current_tick):
        '''
        calculates actual Steering angle based on simple Single Track model -> tick-difference = Neutral Encoder Postion - Current Encoder Tick
        '''
        tick_diff = self.neutral_steering_tick - current_tick
        print(tick_diff)

        if tick_diff > 0: #->rechtskurve
            #print("right")
            return self.angle_per_tick_right
        else: #-> linkskurve
            #print("left")
            return self.angle_per_tick_left
        



if __name__ == '__main__':
    rospy.init_node('encoder_to_odom')
    odom_single_track()
    rospy.spin()
