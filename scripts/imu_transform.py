#! /usr/bin/env python3

import rospy
from sensor_msgs.msg import Imu

def cb_imu_msg(msg:Imu):
    imu_trans = Imu()

    imu_trans.header.stamp = rospy.Time.now()
    imu_trans.header.frame_id = 'imu_rep_frame'

    imu_trans.linear_acceleration.x = msg.linear_acceleration.z
    imu_trans.linear_acceleration.y = msg.linear_acceleration.x * -1
    imu_trans.linear_acceleration.z = msg.linear_acceleration.y * -1

    imu_trans.angular_velocity.x = msg.angular_velocity.z
    imu_trans.angular_velocity.y = msg.angular_velocity.x * -1
    imu_trans.angular_velocity.z = msg.angular_velocity.y * -1

    #imu_trans.linear_acceleration_covariance = msg.linear_acceleration_covariance
    #imu_trans.angular_velocity_covariance = msg.angular_velocity_covariance
    #imu_trans.angular_velocity_covariance[8] = 0.001
    #imu_trans.linear_acceleration_covariance[0] = 0.001

    imu_trans.linear_acceleration_covariance =  [0.001, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.001]
    imu_trans.angular_velocity_covariance =     [0.001, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.001]


    pub_imu.publish(imu_trans)


if __name__ == '__main__':
    rospy.init_node('imu_transform', anonymous=False)
    sub = rospy.Subscriber('/camera/imu', Imu, cb_imu_msg)
    pub_imu = rospy.Publisher('/imu/data_raw', Imu, queue_size=1)
    while(not rospy.is_shutdown()):
        rospy.spin()